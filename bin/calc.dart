import 'dart:io';

class Calculator {
  double sum(double a, double b) {
    return a + b;
  }

  double subtract(double a, double b) {
    return a - b;
  }

  double multiply(double a, double b) {
    return a * b;
  }

  double divide(double a, double b) {
    return a / b;
  }
}

void main(List<String> arguments) {
  Calculator calc = Calculator();
  print("1.Sum");
  print("2.Subtract");
  print("3.Multiply");
  print("4.Divide");
  print("5.Exit");
  String? input = stdin.readLineSync();
  if (input == "5") {
    print("Exit program.");
    exit(0);
  } else {
    print("Enter First number: ");
    String? firstInput = stdin.readLineSync();
    double a = double.parse(firstInput!);
    print("Enter Second number: ");
    String? secondInput = stdin.readLineSync();
    double b = double.parse(secondInput!);
    double result;
    switch (input) {
      case "1":
        result = calc.sum(a, b);
        print("$a + $b = $result");
        break;
      case "2":
        result = calc.subtract(a, b);
        print("$a - $b = $result");
        break;
      case "3":
        result = calc.multiply(a, b);
        print("$a * $b = $result");
        break;
      case "4":
        result = calc.divide(a, b);
        print("$a / $b = $result");
        break;
    }
  }
}
